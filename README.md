Forked from: https://codeberg.org/tenplus1/bakedclay

Fork commit: 67d0f2a8d6bdf88a788421110479057e025bfb85

Baked Clay

This mod lets the player bake clay into hardened blocks and colour them with
dye (8x baked clay and 1x dye in centre), stairs and slabs are also available.

https://forum.minetest.net/viewtopic.php?id=8890

Changelog:

- 0.7 - Added support for stairsplus so that stairs are registered properly
- 0.6 - Added 3 new flowers and a new grass that are used for missing dyes
- 0.5 - Now using minecraft recipe to colour baked clay (8x baked clay, 1x dye in centre)
- 0.4 - Code tweak and tidy
- 0.3 - Added Stairs and Slabs for each colour
- 0.2 - Any colour of baked clay can be re-dyed into another colour
- 0.1 - Initial Release

Lucky Blocks: 8
